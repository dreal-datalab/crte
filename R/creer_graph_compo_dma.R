#' Creation du graphique de repartition des ets RGE selon leur taille
#'
#' @param epci_choisi le code EPCI au format texte
#'
#' @return un graphique ggplot
#' @export
#'
#' @examples
#' creer_graph_compo_dma(epci_choisi = "244400503")
#'
#' @importFrom dplyr mutate filter across contains if_else select arrange desc
#' @importFrom ggplot2 ggplot geom_bar aes labs coord_flip theme guides guide_legend
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom tidyr pivot_longer
#'

creer_graph_compo_dma <- function(epci_choisi = "244400503") {

  ter_ref <- c(paste0("D", get_codes_departement(epci_choisi)), "R52", paste0("E", epci_choisi))

  mil <- get_millesime(crte::data_dechets, "dma.tonnage_collecte") %>% max()

  df <- crte::data_dechets %>%
    dplyr::mutate(id_terr = paste0(substr(.data$TypeZone, 1, 1), .data$CodeZone),
                  TypeZone = factor(.data$TypeZone, levels = c("Epci",  "D\u00e9partements", "R\u00e9gions", "Communes", "France")),
                  dplyr::across(dplyr::contains("tonnage_collecte"), ~ dplyr::if_else(is.na(.data$dma.tonnage_collecte)| dma.tonnage_collecte == 0,
                                                                                    0, .x/.data$dma.tonnage_collecte*100))) %>%
    dplyr::filter(.data$date == mil, .data$id_terr %in% ter_ref) %>%
    dplyr::select(.data$Zone, .data$TypeZone, dplyr::contains("tonnage_collecte"), -.data$dma.tonnage_collecte) %>%
    tidyr::pivot_longer(cols = dplyr::contains("tonnage_collecte"), names_to = "filiere", values_to = "pct",
                        names_transform = list(filiere = ~ factor(.x, levels = c('biodech.tonnage_collecte', 'coll_sep_hs_grav.tonnage_collecte', 'ddangereux.tonnage_collecte',
                                                                                'decheteries.tonnage_collecte', 'encombrants.tonnage_collecte', 'omr.tonnage_collecte',
                                                                                'pap_embal.tonnage_collecte', 'verre.tonnage_collecte'),
                                                                 labels = c('biod\u00e9chets', 'autres d\u00e9chets', 'd\u00e9chets dangereux', 'd\u00e9ch\u00e8teries', 'encombrants',
                                                                            'ordures m\u00e9nag\u00e8res r\u00e9siduelles', 'papiers et emballages', 'verre'))
                        )) %>%
    dplyr::arrange(.data$filiere, dplyr::desc(.data$TypeZone))

  ggplot2::ggplot(df) +
    ggplot2::geom_bar(ggplot2::aes(x=.data$Zone, y=.data$pct, group = .data$Zone, fill = .data$filiere),
                      stat = "identity") +
    ggplot2::labs(x=NULL, y = NULL, caption = "% des tonnes de DMA collect\u00e9es, source : ADEME, enqu\u00eate collecte 2017", fill = NULL,
                  title = "R\u00e9partition des tonnes de d\u00e9chets m\u00e9nagers par fili\u00e8re de collecte") +
    gouvdown::scale_fill_gouv_discrete(name = "", palette = "pal_gouv_qual2", na.value = "grey") +
    ggplot2::coord_flip() + ggplot2::theme(legend.position = "bottom") +
    ggplot2::guides( fill = ggplot2::guide_legend(label.position = 'bottom', keyheight = .2, reverse = FALSE,
                                                  direction = 'horizontal', nrow = 2))


}

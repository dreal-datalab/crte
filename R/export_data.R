#' is_not_empty detection des colonnes vides
#'
#' @param x une variable, ou un vecteur
#' @return un booleen
#'
#' @examples
#' crte:::is_not_empty(c(5, NA, 12))
#' crte:::is_not_empty(c(NA, NA, NA))
is_not_empty <- function(x) {
  !all(is.na(x))
  }


#' mef_onglet : mise un forme d'un jeu de donnees thematique en vue de l'export tableur.
#' Passage au format large (suffixe annee des noms de champs) : une ligne = un territoire
#'
#' @param dataset le dataframe, embarque dans le package, a mettre en forme
#' @return un dataframe avec une ligne par territoire
#' @importFrom dplyr select contains everything arrange
#' @importFrom tidyr pivot_wider
#' @importFrom tidyselect vars_select_helpers
#' @importFrom rlang .data
#' @examples
#' crte:::mef_onglet(data_agriculture)
mef_onglet <- function(dataset) {
  dataset %>%
    dplyr::select(dplyr::contains("Zone"), .data$date, dplyr::everything()) %>%
    tidyr::pivot_wider(id_cols = dplyr::contains("Zone"), names_from = .data$date, values_from = !c(dplyr::contains("Zone"), .data$date),
                names_glue = "{.value}_{date}") %>%
    dplyr::select(tidyselect::vars_select_helpers$where(is_not_empty)) %>%
    dplyr::arrange(.data$Zone)
  }

#' rassemblement_des_onglets
#' @return une liste de df mis en forme, prêt à filtrer en vue de l'export
#' @examples
#' liste_onglets <-crte:::rassemblement_des_onglets()
rassemblement_des_onglets <- function() {

  list(agriculture = mef_onglet(crte::data_agriculture),
       biodiversite = mef_onglet(crte::data_biodiversite),
       conso_espace = mef_onglet(crte::data_consommation_espace),
       dechets = mef_onglet(crte::data_dechets),
       demographie = mef_onglet(crte::data_demo),
       eau = mef_onglet(crte::data_eau),
       energie_climat = mef_onglet(crte::data_energie_climat),
       mobilite = mef_onglet(crte::data_mobilite)
  )
}

#' filtre_epci : Filtrer les onglets pour ne garder que les territoires importants pour l'utilisateur
#' @param x la liste de df mis en forme
#' @param y le code epci (character)
#' @return la liste des onglets filtres sur les territoires importants pour l'utilisateur
#' @importFrom COGiter filtrer_cog
#' @importFrom purrr map
#' @examples
#' crte:::filtre_epci(x = crte:::rassemblement_des_onglets(), y = "244400552")
filtre_epci <- function(x = crte:::rassemblement_des_onglets(), y = "244400552"){
  purrr::map(.x = x, .f = ~ COGiter::filtrer_cog(.x, epci = y, garder_supra = ">="))
}

#' filtre_dep : Filtrer les onglets pour ne garder que les territoires importants pour l'utilisateur
#' @param x la liste de df mis en forme
#' @param y le code dep (character)
#' @return la liste des onglets filtres sur les territoires importants pour l'utilisateur
#' @importFrom COGiter filtrer_cog
#' @importFrom purrr map
#' @examples
#' crte:::filtre_dep(x = crte:::rassemblement_des_onglets(), y = "44")
filtre_dep <- function(x = crte:::rassemblement_des_onglets(), y = "49"){
  purrr::map(.x = x, .f = ~ COGiter::filtrer_cog(.x, dep = y, garder_supra = ">="))
}

#' export_tableur : export des données des portraits CRTE relatives a un departement, un epci ou la region.
#' Par exemple, pour un export departemental : le tableur contient les communes du departement, les epci du departement,
#' l'ensemble des departements de la région et la region.
#'
#' @param chemin character, l'adresse du repertoire de destination de l'export
#' @param type character, type de territoire pour lequel on souhaite exporter les donnees parmi : "epci", "dep" ou "reg".
#' @param code le code du territoire au format texte
#'
#' @return un message confirmant l'export ou indiquant l'echec
#' @importFrom COGiter list_dep_in_reg list_epci_in_reg
#' @importFrom writexl write_xlsx
#' @export
#'
#' @examples
#' export_tableur(type = "reg", code = "52", chemin = tempdir())
export_tableur <- function(type = "dep", code = "44", chemin = "."){
  # verification de la coherence de la saisie
  if(type == "dep" & !(code %in% COGiter::list_dep_in_reg("52"))) {
    stop(paste0(code, " n est pas un code departemental valide"))}
  if(type == "epci" & !(code %in% COGiter::list_epci_in_reg("52"))) {
    stop(paste0(code, " n est pas un code epci valide"))}

  # on crée le répertoire si inexistant
  if(!dir.exists(chemin)) {dir.create(chemin, recursive = TRUE, showWarnings = FALSE)}

  # le chemin du fichier à créer
  chemin_export <- paste0(chemin, "/data_prortraits_CRTE_", substr(type, 1, 1), code, ".xlsx")

  # la fonction de filtre a utiliser
  filtre_ter <- function(a, b) {
    if(type=="epci") {
      filtre_epci(x = a, y = b)
    } else {
      if(type=="dep") {
        filtre_dep(x = a, y = b)
      } else {
        as.list(a)
      }
    }
  }
 # export tableur des données
  rassemblement_des_onglets() %>%
    filtre_ter(b = code) %>%
    writexl::write_xlsx(col_names = TRUE, path = chemin_export)

  # message de confirmation ou d'erreur
  if(file.exists(chemin_export)) {
    message(paste0("le fichier ", file.path(chemin_export)," a ete cree.\n"))
  } else {
    stop("le fichier n\'a pas pu etre exporte. \n")
  }
}

#' Exporter l'ensemble des tableurs de donnees des CRTE d'un coup
#'
#' @param repo character, adresse du repertoire dans lequel les tableurs seront crees
#' @param debug TRUE si on veut tester la fonction en n'exportant que 5 tableurs, FALSE par defaut, si on veut tout les portrait
#'
#' @return les messages de confirmation ou d'erreur lies a chaque export
#' @importFrom COGiter list_epci_in_reg list_dep_in_reg
#' @importFrom purrr map
#' @export
#'
#' @examples
#' exporter_les_tableurs(tempdir(), debug = TRUE)

exporter_les_tableurs <- function(repo = ".", debug = FALSE) {

  liste_epci <- COGiter::list_epci_in_reg("52")
  liste_dep <- COGiter::list_dep_in_reg("52")

  if(debug){
    liste_epci <- liste_epci[1:3]
    liste_dep <- liste_dep[1]
  }

  purrr::map(.x = liste_epci, .f = ~ export_tableur(type = "epci", code = .x, chemin = repo))
  purrr::map(.x = liste_dep,  .f = ~ export_tableur(type = "dep", code = .x, chemin = repo))
  export_tableur(type = "reg", code = "52", chemin = repo)
}


#' Creation du graphique de repartition des ets RGE selon leur taille
#'
#' @param epci_choisi le code EPCI au format texte
#'
#' @return un graphique ggplot
#' @export
#'
#' @examples
#' creer_graph_taille_rge(epci_choisi = "244400503")
#'
#' @importFrom dplyr mutate filter across contains if_else select arrange desc
#' @importFrom ggplot2 ggplot geom_bar aes labs coord_flip theme guides guide_legend
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom tidyr pivot_longer
#'

creer_graph_taille_rge <- function(epci_choisi = "244400503") {

  ter_ref <- c(paste0("D", get_codes_departement(epci_choisi)), "R52", paste0("E", epci_choisi))

  mil <- get_millesime(crte::data_energie_climat, "nb_rge") %>% max()

  # un ets peut être concerné par plusieurs domaine, il faut recréer un total domaines.ets
  df <- crte::data_energie_climat %>%
    dplyr::mutate(id_terr = paste0(substr(.data$TypeZone, 1, 1), .data$CodeZone),
                  TypeZone = factor(.data$TypeZone, levels = c("Epci",  "D\u00e9partements", "R\u00e9gions", "Communes", "France")),
                  dplyr::across(dplyr::contains("rge_taille_"), ~ dplyr::if_else(.data$nb_rge == 0, 0, .x/.data$nb_rge*100))) %>%
    dplyr::filter(.data$date == mil, .data$id_terr %in% ter_ref) %>%
    dplyr::select(.data$Zone, .data$TypeZone, dplyr::contains("rge_taille_")) %>%
    tidyr::pivot_longer(cols = dplyr::contains("rge_taille_"), names_to = "taille", values_to = "pct", names_prefix = "rge_taille_ets_",
                        names_transform = list(taille = ~ factor(.x, levels = c('pme', 'eti', 'ge', 'na'),
                                                                  labels = c('petites ou moyennes entreprises', 'entreprises de taille interm\u00e9diaire', 'grandes entreprises', 'taille inconnue'))
                        )) %>%
    dplyr::arrange(.data$taille, dplyr::desc(.data$TypeZone))

  ggplot2::ggplot(df) +
    ggplot2::geom_bar(ggplot2::aes(x=.data$Zone, y=.data$pct, group = .data$Zone, fill = .data$taille),
                      stat = "identity") +
    ggplot2::labs(x=NULL, y = NULL, caption = "% des \u00e9tablissements certifi\u00e9s, source : ADEME, 2021", fill = NULL,
                  title = "Taille des entreprises certifi\u00e9es RGE") +
    gouvdown::scale_fill_gouv_discrete(name = "", palette = "pal_gouv_qual2", na.value = "grey") +
    ggplot2::coord_flip() + ggplot2::theme(legend.position = "bottom") +
    ggplot2::guides( fill = ggplot2::guide_legend(label.position = 'bottom', keyheight = .2, reverse = FALSE,
                                                  direction = 'horizontal', nrow = 2))


}

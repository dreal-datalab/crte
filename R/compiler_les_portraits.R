#' Construire le nom du fichier portrait
#'
#' @param n_epci le nom de l'epci
#' @param filename le nom de base du document
#'
#' @return le nom du fichier sans l'extension
#' @importFrom dplyr filter mutate
#' @importFrom rlang .data
#' @importFrom tricky str_standardize
#' @export
#'
#' @examples
#' get_name_portrait(n_epci = "CA Saumur Val de Loire", filename = "portrait_crte")

get_name_portrait <- function(n_epci = "Nantes M\u00e9tropole", filename = "portrait_crte") {
  if(n_epci == nom_85113()){paste("85", filename, "l_ile_d_yeu", "85113.docx", sep = ".")} else {
    crte::siege_epci %>%
      dplyr::filter(.data$siren == get_code_epci(n_epci)) %>%
      dplyr::mutate(nom_fich = paste(.data$id_dep, filename, tricky::str_standardize(.data$nom_com), .data$siren, "docx", sep = ".")) %>%
      dplyr::pull(.data$nom_fich)
  }
}




#' compiler les portraits crte pour l'ensemble des EPCI
#'
#' @param dir le répertoire où compiler les portraits
#' @param debug TRUE si on veut compiler seulement 5 portraits
#' @param filename le nom du document
#'
#' @return les portraits sont compilés en .docx
#' @importFrom dplyr filter pull
#' @importFrom stringr str_detect
#' @importFrom rmarkdown render
#' @importFrom stringr str_detect
#' @importFrom utils head
#' @importFrom purrr map
#' @importFrom glue glue
#' @export
#'
compiler_les_portraits <- function(dir = "portraits", debug = FALSE, filename = "portrait_crte") {

  l <- system.file("rmarkdown/templates/portrait_territoire/skeleton/", package = "crte")
  file.copy(from = l, to = ".", recursive = TRUE)

  if (dir.exists(dir)) {
    warning(glue::glue("ce r\u00e9pertoire existe d\u00e9j\u00e0"))
    file.copy(from = "skeleton", to=dir)
    unlink("skeleton")
  }

  if (!dir.exists(dir))
  {
  file.rename(from = "skeleton", to = dir)
  }

  liste_epci <- COGiter::epci %>%
    dplyr::filter(stringr::str_detect(.data$REGIONS_DE_L_EPCI, "52")) %>%
    dplyr::pull(.data$NOM_EPCI) %>%
    as.character() %>%
    c(.data, nom_85113())

  if (debug == TRUE) {
    liste_epci <- sample(liste_epci, 5)
  }

  purrr::map(
    liste_epci,
    ~ rmarkdown::render(paste0(dir,"/skeleton.Rmd"),
      output_file = get_name_portrait(n_epci = .x, filename = "portrait_crte"),
      params = list(nom_epci = .x)
    )
  )

  file.remove(paste0(dir,"/skeleton.Rmd"))
  unlink(paste0(dir,"/template"), recursive = TRUE)
}
#' compiler le portrait crte pour un EPCI
#'
#' @param dir le répertoire où compiler les portraits
#' @param epci le nom de l'epci
#' @param filename le nom du document
#'
#' @return les portraits sont compilés en .docx
#' @importFrom rmarkdown render
#' @import forcats
#' @import ggtext
#' @import lubridate
#' @import knitr
#' @import officedown
#' @import officer
#' @import sf
#' @export
#' @importFrom rmarkdown render
compiler_un_portrait <- function(dir = "portraits", epci = "Nantes M\u00c9tropole", filename = NULL) {
  l <- system.file("rmarkdown/templates/portrait_territoire/skeleton/", package = "crte")
  file.copy(from = l, to = ".", recursive = TRUE)
  file.rename(from = "skeleton", to = dir)
  if (is.null(filename)) {
    filename <- get_name_portrait(n_epci = epci, filename = "portrait_crte")
  }
  rmarkdown::render("report/skeleton/skeleton.Rmd",
    output_file = filename,
    params = list(nom_epci = .x)
  )
}



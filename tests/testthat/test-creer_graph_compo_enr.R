test_that("creer_graph_compo_enr works", {
  objet <- creer_graph_compo_enr(epci_choisi = "244400503")
  testthat::expect_equal(attr(objet, "class"), c("gg", "ggplot"))
})

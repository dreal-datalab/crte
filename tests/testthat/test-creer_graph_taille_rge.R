test_that("creer_graph_taille_rge works", {
  objet <- creer_graph_taille_rge(epci_choisi = "244400503")
  testthat::expect_equal(attr(objet, "class"), c("gg", "ggplot"))
})

## code to prepare `data_crte` dataset goes here
rm(list=ls())
library(purrr)
library(tidyr)
library(dplyr)
library(COGiter)
library(tricky)
library(lubridate)
library(forcats)

ocsge_file <- "inst/extdata/cogifiee_chargement_ocsge.RData"
plui_file <- "inst/extdata/ref_epci_avancement_plui.RData"
obs_artif <- "inst/extdata/cogifiee_chargement_observatoire_artificialisation.RData"
pop_legale <- "inst/extdata/cogifiee_chargement_population_legale.RData"

# une fonction sale pour passer aux communes 2021, en PdL seulement une fusion de communes dans le 53
com_2021 <- function(dataset = data_cogifiee) {
  data_com <- filter(dataset, TypeZone == "Communes") %>%
    select(-Zone, -TypeZone) %>%
    passer_au_cog_a_jour(code_commune = CodeZone, garder_info_supra = F) %>%
    right_join(COGiter::liste_zone %>% select(Zone, CodeZone, TypeZone), .,
               by = c("CodeZone" = "DEPCOM"))

  filter(dataset, TypeZone != "Communes") %>%
    bind_rows(data_com, .)
}


load(ocsge_file)

data_consommation_espace_ocsge <- data_cogifiee %>%
  com_2021()  %>%
  # un pb avec la modalité 'a_definir' de variable
  mutate(variable = gsub("é", "e", variable)) %>%
  filter(variable != "a_definir") %>%
  pivot_wider(names_from = "variable", values_from = "valeur") %>%
  # probleme sur date
  mutate(date = date - years(1)) %>%
  mutate(across(where(is.numeric), ~ replace_na(.x, 0)),
         across(where(is.numeric), ~ .x / 10000)) %>%
  rowwise() %>%
  mutate(taux_artificialisation = 100 * espace_artificialise / sum(c_across(where(is.numeric)))) %>%
  ungroup() %>%
  group_by(TypeZone, Zone, CodeZone) %>%
  mutate(
    evolution_espace_artificialise = espace_artificialise - lag(espace_artificialise),
    evolution_taux_artificialisation = taux_artificialisation - lag(taux_artificialisation)
  ) %>%
  ungroup() %>%
  filter(date == max(date))

load(plui_file)
data_consommation_espace_plui <- avancement_plui %>%
  rename(CodeZone = code_epci) %>%
  mutate(TypeZone = "Epci",
         CodeZone = as.factor(CodeZone)) %>%
  left_join(COGiter::liste_zone %>%
              select(TypeZone,CodeZone,Zone)
            ) %>%
  mutate(code_avancement_plui = as.factor(code_variable) %>%
           fct_recode(
             "PLUi couvrant l\'ensemble de l\'epci" = "1",
             "PLUiH couvrant l\'ensemble de l\'epci" = "2",
             "PLUi partiel ne couvrant pas l\'ensemble de l\'epci" = "3",
             "PLUi transform\u00e9s en PLU suite \u00e0 la creation de communes nouvelles" = "4",
             "Absence de PLUi"="5"
           ),
         date = make_date(2019, 12, 31)) %>%
  select(contains("Zone"), date, code_avancement_plui)

levels(data_consommation_espace_plui$code_avancement_plui) <- enc2utf8(levels(data_consommation_espace_plui$code_avancement_plui))

load(obs_artif)
obs_artif <- data_cogifiee %>%
  com_2021()  %>%
  filter(variable == 'flux_naf_artificialisation_total') %>%
  filtrer_cog(reg = '52', garder_supra = '>') %>%
  pivot_wider(names_from = "variable",
              values_from = "valeur") %>%
  filter(year(date)>=2011) %>%
  group_by(TypeZone,CodeZone,Zone) %>%
  summarise(flux_naf_artificialisation_total = sum(flux_naf_artificialisation_total)) %>%
  ungroup()
load(pop_legale)
pop_legale <- data_cogifiee %>%
  com_2021() %>%
  filtrer_cog(reg = '52',garder_supra = '>') %>%
  filter(year(date)>=2007) %>%
  pivot_wider(names_from = "variable",
              values_from = "valeur") %>%
  group_by(TypeZone,Zone,CodeZone) %>%
  mutate(flux_population_legale = population_municipale-lag(population_municipale)) %>%
  filter(year(date)>=2008) %>%
  group_by(TypeZone,CodeZone,Zone) %>%
  summarise(flux_population_legale = sum(flux_population_legale)) %>%
  ungroup()

data_consommation_espace_obs_artif <- obs_artif %>%
  left_join(pop_legale) %>%
  mutate(flux_naf_artificialisation_par_habitant_supplementaire = flux_naf_artificialisation_total/flux_population_legale,
         flux_naf_artificialisation_par_habitant_supplementaire = ifelse(flux_population_legale<0,NA,flux_naf_artificialisation_par_habitant_supplementaire),
         date = ymd('20190601')) %>%
  select(-c(flux_naf_artificialisation_total,flux_population_legale))

list<-ls()[stringr::str_detect(ls(),"consommation_espace")]

data_consommation_espace <- map(list, ~ get(.x) %>%
                          mutate(date=year(date))) %>%
  reduce(full_join) %>%
  mutate(TypeZone = factor(TypeZone,levels=levels(data_consommation_espace_obs_artif$TypeZone))) %>%
  bind_rows(
    filter(., CodeZone == "85113") %>%
      mutate(TypeZone = factor("Epci", levels = levels(.$TypeZone)),
             CodeZone = factor("85113ZZZZ", levels = c(levels(.$CodeZone), "85113ZZZZ"))), .
  )%>%
  mutate(TypeZone = factor(TypeZone, levels = c("Communes", "Epci", "Départements", "Régions", "France")))

usethis::use_data(data_consommation_espace,overwrite = TRUE)
